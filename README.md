# Private Message Windows

This "Private Message" addon creates floating windows in the lower right corner of the window with the list of threads and allows users to chat on any page of the site, like on Linkedin or Facebook.

## Installation

### 1. Install the module

Install the private_message_windows module as you would any Drupal module.

### 2. Add module block to pages

Open /admin/structure/block and add "Private Message Windows" block to any part of your site layout, for example, to bottom. Change block parameters, such as windows color and background, and choose URLs, where the module will work.


### 3. Change module config if you want

Open /admin/config/private-message/windows-config and add the title and the avatar user field names, etc

### 4. Check permissions

Open /admin/people/permissions and check that authenticated user have permissions: "Use private messaging system" and "View user information"



  



<?php

namespace Drupal\private_message_windows\Plugin\Block;

use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\private_message\Service\PrivateMessageServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Render\HtmlEscapedText;

/**
 * Provides the private message windows block.
 *
 * This block holds windows for private message threads.
 *
 * @Block(
 *   id = "private_message_windows_block",
 *   admin_label = @Translation("Private Message Windows"),
 *   category =  @Translation("Private Message Windows"),
 * )
 */
class PrivateMessageWindowsBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The private message service.
   *
   * @var \Drupal\private_message\Service\PrivateMessageServiceInterface
   */
  protected $privateMessageService;

  /**
   * The CSRF token generator service.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfToken;

  private $formFieldList = ['thread_count', 'title_text_color', 'title_text_background', 'title_new_msg_text_color', 'ajax_refresh_rate', 'default_hidden', 'user_search'];

  /**
   * The language manager service.
   *
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a PrivateMessageWindowsBlock object.
   *
   * @param array $configuration
   *   The block configuration.
   * @param string $plugin_id
   *   The ID of the plugin.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   * @param \Drupal\private_message\Service\PrivateMessageServiceInterface $privateMessageService
   *   The private message service.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrfToken
   *   The CSRF token generator service.
   * @param LanguageManagerInterface $languageManager
   *   Language Manager Service
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountProxyInterface $currentUser,
    ConfigFactory $configFactory,
    PrivateMessageServiceInterface $privateMessageService,
    CsrfTokenGenerator $csrfToken,
    LanguageManagerInterface $languageManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentUser = $currentUser;
    $this->configFactory = $configFactory;
    $this->privateMessageService = $privateMessageService;
    $this->csrfToken = $csrfToken;
    $this->languageManager = $languageManager;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                            array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('private_message.service'),
      $container->get('csrf_token'),
      $container->get('language_manager')
    );
  }


  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function build() {
    $block = [];

    if (!$this->currentUser->isAuthenticated() or
          !$this->currentUser->hasPermission('use private messaging system')) {
      return $block;
    }

//    $pmConfig = $this->configFactory->get('private_message.settings');
//    $url = Url::fromRoute('private_message.private_message_create');
//    $block['links'] = [
//      '#type' => 'link',
//      '#title' => $pmConfig->get("create_message_label"),
//      '#url' => $url,
//    ];

    $block['#cache']['max-age'] = 0;
    $block['#attached']['library'] = [
            'private_message_windows/windows_block_style',
            'private_message_windows/windows_block_script',
    ];

    $blockConfig = $this->getConfiguration();

    $block['title'] = [
      '#theme' => 'private_message_windows__window_header',
      '#addAnotherUserMsg' => $this->t('Open messages of another user.'),
      '#userPicture' => private_message_windows_get_user_thumbnail($this->currentUser),
      '#userSearch' => $blockConfig['user_search'],
    ];

    $block['#attributes']['class'][] = 'block';
    $block['#attributes']['class'][] = 'block-private-message-windows';
    $block['#attributes']['class'][] = 'block-private-message-windows-block';
    $block['#prefix'] = '<div class="block-content">';
    $block['#suffix'] = '</div>';

    $threadInfo = $this->privateMessageService->getThreadsForUser($blockConfig['thread_count']);

    $block['threadList'] = private_message_windows_get_last_threads($threadInfo['threads'], $this->currentUser);
    if(!count($block['threadList'])) $block['threadList'] = [[
      '#prefix' => '<div class="no-thread">',
      '#suffix' => '</div>',
      '#markup' => $this->t('There is still no messages'),
    ]];
    $block['threadList']['#prefix'] = '<div class="inbox-thread-list">';
    $block['threadList']['#suffix'] = '</div><div class="threads-wrapper"></div>';

    //$view_builder = $this->entityTypeManager->getViewBuilder('private_message_thread');
    $route = 'private_message_windows.ajax_callback';
    $prev_threads_url = $this->getUrlWToken($route, 'get_old_inbox_threads');
    $new_threads_url = $this->getUrlWToken($route, 'get_new_inbox_threads');
    $prev_msgs_url = $this->getUrlWToken($route, 'get_old_thread_msgs');
    $new_msgs_url = $this->getUrlWToken($route, 'get_new_thread_msgs');
    $load_thread_msgs_url = $this->getUrlWToken($route, 'load_thread');
    $send_msg_url = $this->getUrlWToken($route, 'send_message');
    $new_thread_url = $this->getUrlWToken($route, 'new_thread');
    $get_users_url = $this->getUrlWToken($route, 'get_users');
    $del_thread_url = $this->getUrlWToken($route, 'delete_thread');

    //$oldestTimestamp = false;
    //if(count($threads) and $threadInfo['next_exists']) {
      //$last_thread = array_pop($threads);
      //$oldestTimestamp = $last_thread->get('updated')->value;
    //}

    $block['#attached']['drupalSettings']['privateMessageWindowsBlock'] = [
      'loadPrevThreadUrl' => $prev_threads_url->toString(),
      'loadNewThreadUrl' => $new_threads_url->toString(),
      'loadPrevMsgsUrl' => $prev_msgs_url->toString(),
      'loadNewMsgsUrl' => $new_msgs_url->toString(),
      'loadThreadMsgsUrl' => $load_thread_msgs_url->toString(),
      'newThreadUrl' => $new_thread_url->toString(),
      'deleteThreadUrl' => $del_thread_url->toString(),
      'deleteThreadMsg' => $this->t('Delete thread'),
      'deleteThreadQuestion' => $this->t('Are you sure you want to delete the thread?'),
      'deleteThreadNote' => $this->t('This action cannot be undone.'),
      'sendMsgUrl' => $send_msg_url->toString(),
      'getUsersUrl' => $get_users_url->toString(),
      'ajaxRefreshRate' => $blockConfig['ajax_refresh_rate'],
      'titleTextColor' => $blockConfig['title_text_color'],
      'titleNewMsgTextColor' => $blockConfig['title_new_msg_text_color'],
      'titleTextBackground' => $blockConfig['title_text_background'],
      //'oldestTimestamp' => $oldestTimestamp,
      'defaultHidden' => $blockConfig['default_hidden'],
      'currentLanguagePrefix' => $this->getLanguagePrefix(),
    ];

    return $block;
  }


  private function getUrlWToken($routeName, $operation) {
    $url = Url::fromRoute($routeName, ['op' => $operation]);
    $token = $this->csrfToken->get($url->getInternalPath());
    $url->setOptions(['absolute' => TRUE, 'query' => ['token' => $token]]);

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'thread_count' => 5,
      'title_text_color' => '#FFFFFF',
      'title_text_background' => '#aaa',
      'title_new_msg_text_color' => 'red',
      'ajax_refresh_rate' => 20,
      'default_hidden' => 0,
      'user_search' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['thread_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of threads to show'),
      '#description' => $this->t('The number of threads to be shown in the block'),
      '#default_value' => $config['thread_count'],
      '#min' => 1,
    ];

    $form['title_text_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Color of head text'),
      '#default_value' => $config['title_text_color'],
    ];

    $form['title_new_msg_text_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Color of head text for new-message status'),
      '#description' => $this->t('Color, when user have new (unread) message'),
      '#default_value' => $config['title_new_msg_text_color'],
    ];

    $form['title_text_background'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Color of head background'),
      '#default_value' => $config['title_text_background'],
    ];

    $form['ajax_refresh_rate'] = [
      '#type' => 'number',
      '#title' => $this->t('Ajax refresh rate'),
      '#default_value' => $config['ajax_refresh_rate'],
      '#min' => 0,
      '#description' => $this->t('The number of seconds after which the inbox should refresh itself. Setting this to a low number will result in more requests to the server, adding overhead and bandwidth. Setting this number to zero will disable ajax refresh, and the inbox will only updated if/when the page is refreshed.'),
    ];

    $form['default_hidden'] = [
      '#title' => $this->t('Hide main window by default'),
      '#type' => 'checkbox',
      '#default_value' => $config['default_hidden'],
      '#description' => $this->t('If checked, message window will be hidden and will only open when a new message is appeared or a message link is clicked'),
    ];

    $form['user_search'] = [
      '#title' => $this->t('User search tool'),
      '#type' => 'checkbox',
      '#default_value' => $config['user_search'],
      '#description' => $this->t('If checked, user search tool ("plus" icon) on the main window is showing.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    foreach ($this->formFieldList as $fldName) {
      $this->configuration[$fldName] = $form_state->getValue($fldName);
    }

  }

  function getLanguagePrefix() {
    $prefixes = $this->configFactory->get('language.negotiation')->get('url.prefixes');
    $language = $this->languageManager
            ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

    return ($prefixes and $language and !empty($prefixes[$language]))
      ? '/' . $prefixes[$language] : '';
  }


}

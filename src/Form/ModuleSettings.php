<?php

namespace Drupal\private_message_windows\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure settings for private_message_windows.
 */
class ModuleSettings extends ConfigFormBase {

  const CONFIG_NAME = 'private_message_windows.settings';

  private $fieldList = [ 'user_name_field', 'user_picture_field', 'message_count_by_step', ];
  private $integerFieldList = [ 'message_count_by_step', ];

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [ self::CONFIG_NAME, ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return str_replace('.', '_',  self::CONFIG_NAME);
  }


  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $form['user_name_field'] = [
      '#title' => $this->t('FieldName of user title'),
      '#type' => 'textfield',
      '#description' => $this->t('You can use your own user field for human title of user, for example "field_title". But if you don\'t want to use it, the module will use default "name" field'),
      '#default_value' => $config->get('user_name_field'),
    ];

    $form['user_picture_field'] = [
      '#title' => $this->t('FieldName of user picture'),
      '#type' => 'textfield',
      '#description' => $this->t('You can use the field of user avatar, for example "field_picture". But if you don\'t want to use it, the module will not use any picture of user'),
      '#default_value' => $config->get('user_picture_field'),
    ];

    $form['message_count_by_step'] = [
      '#title' => $this->t('Count of message to load by one step'),
      '#type' => 'textfield',
      '#description' => $this->t('How many messages will be downloaded during opening the user\'s thread or "load previous" command'),
      '#default_value' => $config->get('message_count_by_step'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    foreach ($this->fieldList as $fldName) {
      $userFld = trim($form_state->getValue($fldName));
      if(in_array($fldName, $this->integerFieldList)) {
        $userFld *= 1;
      }
      $form_state->setValue($fldName, $userFld);
      if(!$userFld) continue;

      $spaceSch = explode(' ', $userFld);
      if (count($spaceSch) > 1) {
        $form_state->setErrorByName($fldName, $this->t('Invalid field name: the field name should not contain a space'));
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config(self::CONFIG_NAME);
    foreach ($this->fieldList as $fieldName) {
      $settings->set($fieldName, $form_state->getValue($fieldName));
    }

    // Save all configurations.
    $settings->save();
    parent::submitForm($form, $form_state);
  }

}

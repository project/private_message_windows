<?php

namespace Drupal\private_message_windows\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\private_message\Entity\PrivateMessage;
use Drupal\private_message\Service\PrivateMessageThreadManagerInterface;
use Drupal\private_message_windows\Ajax\PrivateMessageWindowsDeleteThreadCommand;
use Drupal\private_message_windows\Ajax\PrivateMessageWindowsFillThreadMessagesCommand;
use Drupal\private_message_windows\Ajax\PrivateMessageWindowsInboxUpdateCommand;
use Drupal\private_message_windows\Ajax\PrivateMessageWindowsInsertOldMessagesCommand;
use Drupal\private_message\Entity\PrivateMessageThread;
use Drupal\private_message\Service\PrivateMessageServiceInterface;
use Drupal\private_message_windows\Ajax\PrivateMessageWindowsInsertNewMessagesCommand;
use Drupal\private_message_windows\Ajax\PrivateMessageWindowsShowUsersCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller to handle Ajax requests.
 */
class AjaxController extends ControllerBase{

  const AUTOCOMPLETE_COUNT = 5;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The private message thread manager.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $threadStorage;

  /**
   * The private message service.
   *
   * @var \Drupal\private_message\Service\PrivateMessageServiceInterface
   */
  protected $privateMessageService;

  /**
   * The private message service.
   *
   * @var \Drupal\private_message\Service\PrivateMessageThreadManagerInterface
   */
  protected $privateMessageThreadManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;


  /**
   * Constructs n AjaxController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\private_message\Service\PrivateMessageServiceInterface $privateMessageService
   *   The private message service.
   * @param \Drupal\private_message\Service\PrivateMessageThreadManagerInterface $privateMessageThreadManager
   *   The private message service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(
    RendererInterface $renderer,
    RequestStack $requestStack,
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory,
    AccountProxyInterface $currentUser,
    PrivateMessageServiceInterface $privateMessageService,
    PrivateMessageThreadManagerInterface $privateMessageThreadManager,
    Connection $database
  ) {
    $this->renderer = $renderer;
    $this->requestStack = $requestStack;
    $this->entityTypeManager = $entityTypeManager;
    $this->threadStorage = $entityTypeManager->getStorage('private_message_thread');
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
    $this->privateMessageService = $privateMessageService;
    $this->privateMessageThreadManager = $privateMessageThreadManager;
    $this->database = $database;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('private_message.service'),
      $container->get('private_message.thread_manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxCallback($op) {
    $response = new AjaxResponse();

    if ($this->currentUser->hasPermission('use private messaging system')) {
      switch ($op) {

        case 'get_new_inbox_threads':
          $this->getNewInboxThreads($response);
          break;
        case 'get_old_inbox_threads':
          $this->getOldInboxThreads($response);
          break;
        case 'load_thread':
          $this->loadThread($response);
          break;
        case 'get_new_thread_msgs':
          $this->getNewPrivateMessages($response);
          break;
        case 'get_old_thread_msgs':
          $this->getOldPrivateMessages($response);
          break;
        case 'send_message':
          $this->sendMessage($response);
          break;
        case 'new_thread':
          $this->newThread($response);
          break;
        case 'delete_thread':
          $this->deleteThread($response);
          break;
        case 'get_users':
          $this->getUsers($response);
          break;
      }
    }

    return $response;
  }


  /**
   * Creates an Ajax Command with new threads for the private message inbox.
   *
   * @param Drupal\Core\Ajax\AjaxResponse $response
   *   The response to which any commands should be attached.
   */
  protected function getNewInboxThreads(AjaxResponse $response) {
    $ids = $this->requestStack->getCurrentRequest()->get('ids');

    // Check to see if any thread IDs were POSTed.
    if (is_array($ids) and count($ids)) {
      // Get new inbox information based on the posted IDs.
      $inbox_threads = $this->privateMessageService->getUpdatedInboxThreads($ids);
    } else {
      // No IDs were posted, so the maximum possible number of threads to be
      // returned is retrieved from the block settings.
      $thread_count = $this->configFactory->get('block.block.privatemessageinbox')->get('settings.thread_count');
      $inbox_threads = $this->privateMessageService->getUpdatedInboxThreads([], $thread_count);
    }

    $threadList = private_message_windows_get_last_threads($inbox_threads['new_threads'], $this->currentUser);

    // Only need to do something if any thread IDS were found.
    if (count($threadList)) {
      //$view_builder = $this->entityTypeManager->getViewBuilder('private_message_thread');

      // Render any new threads as HTML to be sent to the browser.
      $rendered_threads = [];
      //foreach (array_keys($inbox_threads['new_threads']) as $thread_id) {
      foreach ($threadList as $thread_id => $renderable_thread) {
        //$renderable_thread = $view_builder->view($inbox_threads['new_threads'][$thread_id], 'inbox');
        $rendered_threads[$thread_id] = $this->renderer->renderRoot($renderable_thread);
      }

      // Add the command that will tell the inbox which thread items to update.
      $response->addCommand(new PrivateMessageWindowsInboxUpdateCommand(array_keys($threadList), $rendered_threads));
    }
  }

  /**
   * @param $messages PrivateMessage[]
   *
   * @return array
   */
  protected function getRenderableMessages($messages) {
    $viewBuilder = $this->entityTypeManager->getViewBuilder('private_message');
    $renderable = [];
    foreach ($messages as $message) {
      if (!$message->access('view', $this->currentUser)) continue;
      $renderable[] = $viewBuilder->view($message, 'windows_inbox');
    }
    return $renderable;
  }


  /**
   * Load a private message thread to be dynamically inserted into the page.
   *
   * @param Drupal\Core\Ajax\AjaxResponse $response
   *   The response to which any commands should be attached.
   */
  protected function loadThread(AjaxResponse $response) {
    $thread_id = $this->requestStack->getCurrentRequest()->get('threadid');
    if ($thread_id) {
      $threadStorage = $this->entityTypeManager->getStorage('private_message_thread');
      $thread = $threadStorage->load($thread_id);

      $this->runBeginThread($response, $thread);

    }
  }

  /**
   * Get Thread message list by count (without preloading of all contents of each message)
   *
   * @param PrivateMessageThread $thread
   * @param int $count
   * @param int $fromId - get message after this
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getThreadMessagesSlice($thread, $count = 0, $fromId=0) {

    $idList = array_map(function ($a){ return $a['target_id']; }, $thread->get('private_messages')->getValue());
    if($fromId) {
      rsort($idList, SORT_NUMERIC);
      $key = array_search($fromId, $idList);
      $from = ($key === false)? 0 : $key+1;
      $threadMsgIds = array_slice($idList, $from, $count);
    } elseif($count) {
      $threadMsgIds = array_slice($idList, -1*$count);
    } else {
      $threadMsgIds = $idList;
    }
    if(!$threadMsgIds) return [];

    $storage = $this->entityTypeManager->getStorage('private_message');
    $messages = $storage->loadMultiple($threadMsgIds);
    $deleteTimestamp = $thread->getLastDeleteTimestamp($this->currentUser);

    $result = [];
    foreach ($messages as $i=>$msg) {
      if ($msg->access('view', $this->currentUser) and
                                  $msg->getCreatedTime() > $deleteTimestamp) {
        $result[] = $msg;
      }
    }
    return $result;
  }

  /**
   * Filling Thread data and run command to draw it
   *
   * @param AjaxResponse $response
   * @param PrivateMessageThread $thread
   */
  protected function runBeginThread(AjaxResponse $response, $thread) {
    //if (!$thread or !$thread->access('view', $this->currentUser)) return; //wrong way! it is not working!
    if (!$thread or !in_array($this->currentUser->id(), $thread->getMembersId())) return;

    $this->privateMessageService->updateLastCheckTime();
    $thread->updateLastAccessTime($this->currentUser);

    $messageCnt = $this->configFactory->get('private_message_windows.settings')->get('message_count_by_step');
    $messages = $this->getThreadMessagesSlice($thread, $messageCnt);

    $renderable = $this->getRenderableMessages($messages);
    $rendered_thread = $this->renderer->renderRoot($renderable);

    $firstMsgId = count($messages) ? $messages[0]->id() : 0;
    $lastMessage = array_pop($messages);
    $lastMsgId = $lastMessage? $lastMessage->id() : 0;

    $users = $thread->getMembers();
    $userPicture = $userName = '';
    $userId = 0;
    foreach ($users as $tuser) {
      if($tuser->id() == $this->currentUser->id()) continue;
      $userId = $tuser->id();
      $userPicture = private_message_windows_get_user_thumbnail($tuser);
      $userName = private_message_getUserTitle($tuser);
    }

    //unset($renderable["#attached"]["library"]);
    //$response->setAttachments($renderable['#attached']);

    $response->addCommand(new PrivateMessageWindowsFillThreadMessagesCommand($rendered_thread,
          $thread->id(), $firstMsgId, $lastMsgId, $userId, $userName, $userPicture));

  }


  /**
   * Creates an Ajax Command containing new private message.
   *
   * @param Drupal\Core\Ajax\AjaxResponse $response
   *   The response to which any commands should be attached.
   */
  protected function getNewPrivateMessages(AjaxResponse $response) {
    $thread_id = $this->requestStack->getCurrentRequest()->get('thread_id');
    $lastMessageId = $this->requestStack->getCurrentRequest()->get('message_id');

    if (!is_numeric($thread_id) or !is_numeric($lastMessageId)) return;

    $thread = $this->threadStorage->load($thread_id);
    if (!$thread) return;

    $this->getNewPrivateMessagesAction($response, $thread, $lastMessageId);
  }


  protected function getNewPrivateMessagesAction($response, $thread, $lastMessageId) {
    $new_messages = $this->privateMessageService->getNewMessages($thread->id(), $lastMessageId);
    $this->privateMessageService->updateThreadAccessTime($thread);
    $renderable = [];

    $lastMessageId = 0;
    if (count($new_messages)) {
      $renderable = $this->getRenderableMessages($new_messages);
      $lastMessage = array_pop($new_messages);
      $lastMessageId = $lastMessage->id();

      // Ensure the browser knows the thread ID at all times.
      $renderable['#attached']['drupalSettings']['privateMessageThread']['threadId'] = (int) $thread->id();
    }

    $response->addCommand(new PrivateMessageWindowsInsertNewMessagesCommand($this->renderer->renderRoot($renderable), (int) $thread->id(), $lastMessageId));

  }


  /**
   * Create an Ajax Command containing old private messages threads.
   *
   * @param Drupal\Core\Ajax\AjaxResponse $response
   *   The response to which any commands should be attached.
   */
  protected function getOldInboxThreads(AjaxResponse $response) {

  }


  /**
   * Add new message and create ajax-command to add new messages to the thread
   *
   * @param Drupal\Core\Ajax\AjaxResponse $response
   *   The response to which any commands should be attached.
   */
  protected function sendMessage(AjaxResponse $response) {
    $current_request = $this->requestStack->getCurrentRequest();
    $thread_id = $current_request->get('thread_id');
    $text = $current_request->get('text');
    $lastMessageId = $current_request->get('last_message_id');

    if(!is_numeric($thread_id)) return;
    if(!is_numeric($lastMessageId)) return;
    if(!$text) return;

    /** @var PrivateMessageThread $thread */
    $thread = $this->threadStorage->load($thread_id);
    if (!$thread) return;

    $privateMessage = PrivateMessage::create();
    $privateMessage->set('message', $text);
    $privateMessage->setOwnerId($this->currentUser->id());
    $privateMessage->save();

    $members = [];
    foreach ($thread->getMembers() as $user) {
      $members[] = $user;
    }

    $this->privateMessageThreadManager->saveThread($privateMessage, $members, NULL, $thread);

    $this->getNewPrivateMessagesAction($response, $thread, $lastMessageId);

  }


  /**
   * Find thread by recipient id or create new thread for user
   *
   * @param AjaxResponse $response
   *   The response to which any commands should be attached.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function newThread(AjaxResponse $response) {
    $current_request = $this->requestStack->getCurrentRequest();
    $uid = (int)$current_request->get('recip_id');
    if(!$uid) return;

    $thread = $this->privateMessageService->getThreadForMembers([$this->currentUser, $this->entityTypeManager->getStorage('user')->load($uid)]);
    $this->runBeginThread($response, $thread);
  }

  /**
   * Delete the thread for current user
   *
   * @param AjaxResponse $response
   *   The response to which any commands should be attached.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function deleteThread(AjaxResponse $response) {
    $current_request = $this->requestStack->getCurrentRequest();
    $threadId = (int)$current_request->get('thread_id');
    if(!$threadId) return;

    $thread = $this->threadStorage->load($threadId);
    if($thread) $thread->delete($this->currentUser);

    $response->addCommand(new PrivateMessageWindowsDeleteThreadCommand($thread->id()));
  }


  /**
   * Create an Ajax Command containing old private messages.
   *
   * @param AjaxResponse $response
   *   The response to which any commands should be attached.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getOldPrivateMessages(AjaxResponse $response) {
    $current_request = $this->requestStack->getCurrentRequest();
    $thread_id = $current_request->get('thread_id');
    $message_id = $current_request->get('message_id');

    if (!is_numeric($thread_id) or !is_numeric($message_id)) return;
    $msgCount = $this->configFactory->get('private_message_windows.settings')->get('message_count_by_step');

    $threadStorage = $this->entityTypeManager->getStorage('private_message_thread');
    $thread = $threadStorage->load($thread_id);

    $messages = $this->getThreadMessagesSlice($thread, $msgCount, $message_id);
    $has_next = 0;

    if (count($messages)) {
      $renderable = $this->getRenderableMessages($messages);
      $renderedMessages = $this->renderer->renderRoot($renderable);

      $firstMsgId = count($messages) ? $messages[0]->id() : 0;

      $response->addCommand(new PrivateMessageWindowsInsertOldMessagesCommand($renderedMessages, $thread_id, $firstMsgId, $has_next));
    } else {
      $response->addCommand(new PrivateMessageWindowsInsertOldMessagesCommand('', $thread_id, 0, FALSE));
    }
  }

  /**
   * Get user list for user search line
   *
   * @param AjaxResponse $response
   */
  private function getUsers(AjaxResponse $response) {
    $current_request = $this->requestStack->getCurrentRequest();
    $namePart = $current_request->get('user_name');
    $users = [];

    if($namePart) $users = $this->getUsersByNamePart($namePart);

    $response->addCommand(new PrivateMessageWindowsShowUsersCommand($users));
  }

  /**
   * @param string $userNamePart - the part of users names
   * @return array - user list
   */
  private function getUsersByNamePart($userNamePart) {
    $userNameFld = $this->configFactory->get('private_message_windows.settings')->get('user_name_field');

    $user_ids = $this->getUserIdsFromString($userNamePart, $userNameFld, self::AUTOCOMPLETE_COUNT);
    $accounts = $this->entityTypeManager->getStorage('user')->loadMultiple($user_ids);

    $userList = [];
    foreach ($accounts as $account) {
      if (!$account->access('view', $this->currentUser)) continue;

      $userName = $account->getDisplayName();
      if($userNameFld) {
        $userNameVal = $account->get($userNameFld)->getValue();
        if(!empty($userNameVal[0]['value'])) $userName = $userNameVal[0]['value'];
      }
      $userList[] = [
          'uid' => $account->id(),
          'username' => $userName,
      ];
    }
    return $userList;
  }


  /**
   * Database request for user id searching by substring
   *
   * @param $string - search line
   * @param $userNameFld - database field name of the user title
   * @param $count - number of the results
   * @return array - uid list
   */
  private function getUserIdsFromString($string, $userNameFld, $count) {
    if (!$this->currentUser->hasPermission('access user profiles') or !$this->currentUser->hasPermission('use private messaging system')) {
      return [ ];
    }

    $q = $this->database->select('users_field_data', 'ud');
    $q->leftJoin('user__roles', 'ur', 'ur.entity_id = ud.uid and ur.deleted = 0');
    $q->leftJoin('config', 'rc', 'rc.name = CONCAT(\'user.role.\', ur.roles_target_id)');
    $q->leftJoin('config', 'c', 'c.name = :authenticated_config', [
      ':authenticated_config' => 'user.role.authenticated',
    ]);
    if($userNameFld) {
      $q->leftJoin('user__' . $userNameFld, 'un', 'un.entity_id = ud.uid and un.deleted = 0');
      $q->condition('un.' . $userNameFld . '_value', $this->database->escapeLike($string) . '%', 'LIKE');
    } else {
      $q->condition('ud.name', $this->database->escapeLike($string) . '%', 'LIKE');
    }

    $q->condition('ud.uid', $this->currentUser->id(), '!=');

    $orGroup = $q->orConditionGroup()
      ->condition('c.data', '%s:28:"use private messaging system"%', 'LIKE')
      ->condition('rc.data', '%s:28:"use private messaging system"%', 'LIKE');
    $q->condition($orGroup);

    $q->fields('ud', ['uid']);
    $q->orderBy('ud.name', 'ASC');
    $q->range(0, $count);
//    ksm($q.'');
//    ksm($q->getArguments());
    $res = $q->execute()->fetchCol();
//    ksm($res);
    return $res;
  }


}

<?php

namespace Drupal\private_message_windows\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class to insert new messages into a private message thread.
 */
class PrivateMessageWindowsShowUsersCommand implements CommandInterface {

  protected $users;

  /**
   * @param array $users
   *   The HTML for the messages to be inserted in the page.
   */
  public function __construct($users) {
    $this->users = $users;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'PrivateMessageShowUserList',
      'users' => $this->users,
    ];
  }

}

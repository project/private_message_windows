<?php

namespace Drupal\private_message_windows\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class to insert new messages into a private message thread.
 */
class PrivateMessageWindowsInsertNewMessagesCommand implements CommandInterface {

  protected $threadId;
  protected $messages;
  protected $lastMessageId;
  /**
   * @param string $messages
   *   The HTML for the messages to be inserted in the page.
   */
  public function __construct($messages, $threadId, $lastMessageId) {
    $this->threadId = $threadId;
    $this->lastMessageId = $lastMessageId;
    $this->messages = $messages;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'PrivateMessageThreadMsgsUpdate',
      'messages' => $this->messages,
      'threadId' => $this->threadId,
      'lastMessageId' => $this->lastMessageId,
    ];
  }

}

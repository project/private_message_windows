<?php

namespace Drupal\private_message_windows\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

//use Drupal\private_message\Ajax\PrivateMessageInsertMessagesCommand;

/**
 * Class to insert new messages into a private message thread.
 */
class PrivateMessageWindowsFillThreadMessagesCommand implements CommandInterface {

  use StringTranslationTrait;

  protected $threadId;
  protected $firstMsgId;
  protected $lastMsgId;
  protected $userId;
  protected $userName;
  protected $userPicture;
  protected $messages;
  /**
   * @param string $messages
   *   The HTML for the messages to be inserted in the page.
   */
  public function __construct($messages, $threadId, $firstMsgId, $lastMsgId, $userId, $userName, $userPicture) {
    $this->threadId = $threadId;
    $this->firstMsgId = $firstMsgId;
    $this->lastMsgId = $lastMsgId;
    $this->userId = $userId;
    $this->userName = $userName;
    $this->userPicture = $userPicture;
    $this->messages = $messages;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'PrivateMessageWindowsFillThread',
      'threadId' => $this->threadId,
      'firstMsgId' => $this->firstMsgId,
      'lastMsgId' => $this->lastMsgId,
      'userId' => $this->userId,
      'userName' => $this->userName,
      'userPicture' => $this->userPicture,
      'messages' => $this->messages,
      'loadPrevTitle' => $this->t('Load previous'),
    ];
  }

}

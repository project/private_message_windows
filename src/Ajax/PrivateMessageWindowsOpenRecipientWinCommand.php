<?php

namespace Drupal\private_message_windows\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class to open a new private message thread.
 */
class PrivateMessageWindowsOpenRecipientWinCommand implements CommandInterface {

  protected $threadId;
  protected $messages;
  protected $lastMessageId;

  /**
   * @param string $messages
   *   The HTML for the messages to be inserted in the page.
   */
  public function __construct($messages, $threadId, $lastMessageId) {
    $this->threadId = $threadId;
    $this->lastMessageId = $lastMessageId;
    $this->messages = $messages;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'PrivateMessageOpenRecipientWin',
      'messages' => $this->messages,
      'threadId' => $this->threadId,
      'lastMessageId' => $this->lastMessageId,
    ];
  }

}

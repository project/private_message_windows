<?php

namespace Drupal\private_message_windows\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class to insert new messages into a private message thread.
 */
class PrivateMessageWindowsInsertOldMessagesCommand implements CommandInterface {

  protected $threadId;
  protected $messages;
  protected $firstMessageId;
  protected $hasNext;
  /**
   * @param string $messages
   *   The HTML for the messages to be inserted in the page.
   */
  public function __construct($messages, $threadId, $firstMessageId, $hasNext) {
    $this->threadId = $threadId;
    $this->firstMessageId = $firstMessageId;
    $this->messages = $messages;
    $this->hasNext = $hasNext;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'PrivateMessageInsThreadOldMessages',
      'messages' => $this->messages,
      'threadId' => $this->threadId,
      'firstMessageId' => $this->firstMessageId,
      'hasNext' => $this->hasNext,
    ];
  }

}

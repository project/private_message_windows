<?php

namespace Drupal\private_message_windows\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class to insert new messages into a private message thread.
 */
class PrivateMessageWindowsDeleteThreadCommand implements CommandInterface {

  protected $threadId;

  /**
   * @param integer $threadId
   *   Deleting thread id
   */
  public function __construct($threadId) {
    $this->threadId = $threadId;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'PrivateMessageDeleteThread',
      'thread_id' => $this->threadId,
    ];
  }

}
